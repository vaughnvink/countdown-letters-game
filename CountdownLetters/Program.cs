﻿using CountDownLetters;
using System;

namespace CountdownLetters {
    /// <summary>
    /// Class <c>Program</c> is the main runnable of the program, it creates a Game object and runs it.
    /// </summary>
    class Program {
        static void Main(string[] args) {
            /*
             * References:
             * 
             * English word list used: https://www.wordgamedictionary.com/sowpods/download/sowpods.txt
             * Orginally used in Scrabble, preprocessed with Notepad++ to clean up and remove all words longer than 9 letters.
             * 
             * Ruleset from: https://en.wikipedia.org/wiki/Countdown_(game_show)#Letters_round
             * 
             * Game letter distribution from: http://www.thecountdownpage.com/letters.htm (Jan 2009)
             * 
             */
            Game countdownLettersGame = new Game();
            countdownLettersGame.Play();
            Console.Read();
        }
    }
}
