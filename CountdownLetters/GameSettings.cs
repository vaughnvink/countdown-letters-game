﻿using System;
using System.Collections.Generic;

namespace CountdownLetters {
    /// <summary>
    /// Class <c>GameSettings</c> contains all the settings used by various classes throughout the program.
    /// Synced between all threads and classes.
    /// </summary>
    public sealed class GameSettings {
        //Start of settings
        //Number of rounds played
        public const int roundsNumber = 4;
        //Time limit for each round in ms
        public const int roundTimeLimit = 30000;
        //Minimum letters any answer should have.
        public const int minimumLetters = 0;
        //Maximum letters any answer may have.
        public const int maximumLetters = 9;
        //Path to the game's dictionary. 
        public const String corpusPath = "editedListofWords.txt";
        //Number of vowels the user is required to pick
        public const int minimumVowels = 4;
        //Number of consonants the user is required to pick
        public const int minimumConsonants = 3;
        //Available vowels & consonants and times one can occur from the start of a game
        public static Dictionary<char, int> vowelSettings = new Dictionary<char, int>(){
            { 'A', 15},
            { 'E', 21},
            { 'I', 13},
            { 'O', 13},
            { 'U', 5}
        };
        public static Dictionary<char, int> consonantSettings = new Dictionary<char, int>(){
            {'B', 2},
            {'C', 3},
            {'D', 6},
            {'F', 2},
            {'G', 3},
            {'H', 2},
            {'J', 1},
            {'K', 1},
            {'L', 5},
            {'M', 4},
            {'N', 8},
            {'P', 4},
            {'Q', 1},
            {'R', 9},
            {'S', 9},
            {'T', 9},
            {'V', 1},
            {'W', 1},
            {'X', 1},
            {'Y', 1},
            {'Z', 1}
        };
        //End of settings.

        //Singleton support below.
        private static GameSettings instance = null;
        private static readonly object padlock = new object();

        GameSettings() {
            
        }

        /// <summary>
        /// Method <c>Instance</c> singleton support, returns instance of GameSettings with multi-threaded locking.
        /// </summary>
        public static GameSettings Instance {
            get {
                lock (padlock) {
                    if (instance == null) {
                        instance = new GameSettings();
                    }
                    return instance;
                }
            }
        }
    }
}
