﻿using CountdownLetters;
using System;

namespace CountDownLetters {
    /// <summary>
    /// Class <c>Game</c> is an entire game of Countdown: Letters, uses GameSettings singleton for its options.
    /// </summary>
    public class Game {
        private static WordDictionary words = new WordDictionary();
        private static GamePile gameWords = new GamePile();
        private Round[] rounds = new Round[GameSettings.roundsNumber];

        public int TotalScore { get; set; }
        public int TotalBestScore { get; set; }

        public Game() {}

        /// <summary>
        /// Method <c>Play</c> runs the game by playing through each round, also displays round and score information.
        /// </summary>
        public void Play() {
            DisplayHeader();
            for (int i = 0; i < rounds.Length; i++) {
                rounds[i] = new Round(gameWords, words);

                Console.WriteLine("*** Round {0}/{1} ***", i + 1, rounds.Length);

                rounds[i].PlayRound();
                TotalScore += rounds[i].Score;
                TotalBestScore += rounds[i].BestScore;
                Console.WriteLine("*** Total Score: {0} ***", TotalScore);
            }
            DisplayFooter();
        }
        /// <summary>
        /// Method <c>DisplayHeader</c>
        /// writes a short welcoming text for the user to the console.
        /// </summary>
        private void DisplayHeader() {
            Console.WriteLine("***********************************************************");
            Console.WriteLine("Welcome to the Countdown: Letters game!");
            Console.WriteLine("***********************************************************");
        }
        /// <summary>
        /// Method <c>DisplayFooter</c>
        /// writes a short scoreboard and closing text for the user to the console.
        /// </summary>
        private void DisplayFooter() {
            Console.WriteLine("***********************************************************");
            Console.WriteLine("Your final score is {0}.\nThe best possible score was {1}", TotalScore, TotalBestScore);
            Console.WriteLine("Thank you for playing!");
            Console.WriteLine("***********************************************************");
        }
    }
}

