﻿using CountdownLetters;
using System;
using System.Linq;
using System.Threading;

namespace CountDownLetters {
    /// <summary>
    /// Class <c>Round</c> is a full round that is played in a game.
    /// It handles score calculation and contains a Play() method that runs an entire round.
    /// </summary>
    public class Round {
        private UserPile selectedLetters;
        private GamePile gameLetters;
        private WordDictionary wiseBookOfWords;

        /// <summary>
        /// Constructor for <c>Round</c>
        /// </summary>
        /// <param name="gameLetters">GamePile used for round.</param>
        /// <param name="wiseBookOfWords">Word list used for round.</param>
        public Round(GamePile gameLetters, WordDictionary wiseBookOfWords) {
            Score = 0;
            BestScore = 0;
            this.gameLetters = gameLetters;
            this.wiseBookOfWords = wiseBookOfWords;
            selectedLetters = new UserPile();
            UserAnswer = "";
        }
        
        public int Score { get; set; }
        public int BestScore { get; set; }
        public String BestAnswer { get; set; }
        public String UserAnswer { get; set; }

        /// <summary>
        /// Method <c>CalculateScore</c> calculates the score of a String and returns this in int form.
        /// </summary>
        /// <param name="word">String to calculate score from.</param>
        /// <returns>Score as int.</returns>
        public int CalculateScore(String word) {
            int returned = word.Length;
            if(word.Length == GameSettings.maximumLetters) {
                returned *= 2;
            }
            return returned;
        }

        /// <summary>
        /// Method <c>PlayRound</c> displays & runs all parts of a round:
        /// Building the user's available vowel & consonant piles.
        /// Handling timed input.
        /// Checking and scoring answers.
        /// </summary>
        public void PlayRound() {
            //While more less than the maximum number of letters, ask for letter type and add random letter.
            while (selectedLetters.CanAddMoreLetters()) {
                Console.WriteLine("Would you like a vowel(v) or consonant(c)?");
                String input = Reader.ReadLine();
                if (input.Equals("v", StringComparison.OrdinalIgnoreCase) || input.Equals("vowel", StringComparison.OrdinalIgnoreCase)) {
                    if (!selectedLetters.Add(gameLetters.PopRandomVowel())) {
                        Console.WriteLine("Cannot add another vowel!");
                    }
                } else if (input.Equals("c", StringComparison.OrdinalIgnoreCase) || input.Equals("consonant", StringComparison.OrdinalIgnoreCase)) {
                    if (!selectedLetters.Add(gameLetters.PopRandomConsonant())) {
                        Console.WriteLine("Cannot add another consonant!");
                    }
                }
                Console.WriteLine("Current letters: {0}",selectedLetters);
            }
            //Display final letters
            Console.WriteLine("Your final letters are: {0}", selectedLetters);
            //Timed ReadLine
            try {
                Console.WriteLine("Enter your word and submit using Enter within the next {0} seconds!",GameSettings.roundTimeLimit/1000);
                UserAnswer = Reader.ReadLine(GameSettings.roundTimeLimit);
                UserAnswer = new String(UserAnswer.Where(Char.IsLetter).ToArray()).ToLower();

                Console.WriteLine("Your answer, {0}", UserAnswer);

                //Check answer with the word list and whether all letters used are available.
                if (wiseBookOfWords.IsWordInDictionary(UserAnswer) && UserAnswer.Length >= GameSettings.minimumLetters) {
                    if (selectedLetters.IsWordAccordingToPile(UserAnswer)) {
                        Score = CalculateScore(UserAnswer);
                    } else {
                        Console.WriteLine("Answer disqualified, contained invalid letters.");
                    }
                } else {
                    Console.WriteLine("Answer disqualified, is not present in dictionary.");
                }
            } catch (TimeoutException) {
                Console.WriteLine("Time is up! No answer submitted.");
            }

            //Calculate best answer, display score and best answer.
            BestAnswer = wiseBookOfWords.Solve(selectedLetters.ToString().ToLower())[0];
            BestScore = CalculateScore(BestAnswer);
            Console.WriteLine("You have been awarded {0} points!", Score);
            Console.WriteLine("The longest possible word was: {0}", BestAnswer);

        }
    }
    /// <summary>
    /// Class <c>Reader</c> can read from the Console with a specific timeout attached to it.
    /// </summary>
    class Reader {
        private static Thread inputThread;
        private static AutoResetEvent getInput, gotInput;
        private static string input;

        static Reader() {
            getInput = new AutoResetEvent(false);
            gotInput = new AutoResetEvent(false);
            inputThread = new Thread(reader);
            inputThread.IsBackground = true;
            inputThread.Start();
        }

        private static void reader() {
            while (true) {
                getInput.WaitOne();
                input = Console.ReadLine();
                gotInput.Set();
            }
        }

        /// <summary>
        /// Method <c>ReadLine</c> reads from the console with a timeout. Returns String. Throws TimeOutException.
        /// </summary>
        /// <param name="timeOutMillisecs">Timeout time in ms.</param>
        /// <returns>Line read from the console.</returns>
        public static string ReadLine(int timeOutMillisecs = Timeout.Infinite) {
            getInput.Set();
            bool success = gotInput.WaitOne(timeOutMillisecs);
            if(success) {
                return input;
            } else {
                throw new TimeoutException("User did not provide input within the timelimit.");
            }
        }
    }
}

