﻿using CountdownLetters;
using System;
using System.Collections.Generic;

namespace CountDownLetters {
    /// <summary>
    /// Class <c>GamePile</c> extends LetterPile, forms a collection of consonants and vowels.
    /// Which are distributed according to the vowel and consonant settings in the GameSettings class.
    /// Contains methods to get a random letter from these collections.
    /// </summary>
    public class GamePile : LetterPile {
        private static Random _rnd = new Random();

        private int totalVowelWeight = 0;
        private int totalConsonantWeight = 0;

        public GamePile() {
            VowelPile = new Dictionary<char, int>(GameSettings.vowelSettings);
            ConsonantPile = new Dictionary<char, int>(GameSettings.consonantSettings);

            totalVowelWeight = GetPileLength(VowelPile);
            totalConsonantWeight = GetPileLength(ConsonantPile);
        }

        /// <summary>
        /// Method <c>PopRandomVowel</c> fetches a random vowel from the collections kept inside the GamePile class.
        /// Also removes one instance of the letter from the game's pile.
        /// </summary>
        /// <returns>(Char) Random vowel.</returns>
        public char PopRandomVowel() {

            int randomNumber = _rnd.Next(0, totalVowelWeight);

            char foundKey = '0';

            foreach (KeyValuePair<char, int> kvp in VowelPile) {
                if (randomNumber < kvp.Value) {
                    foundKey = kvp.Key;
                    VowelPile[kvp.Key] = kvp.Value-1;
                    totalVowelWeight += -1;
                    break;
                }

                randomNumber = randomNumber - kvp.Value;
            }

            return foundKey;
        }

        /// <summary>
        /// Method <c>PopRandomConsonant</c> fetches a random consonant from the collections kept inside the GamePile class.
        /// Also removes one instance of the letter from the game's pile.</summary>
        /// <returns>(Char) Random consonant.</returns>
        public char PopRandomConsonant() {

            int randomNumber = _rnd.Next(0, totalConsonantWeight);

            char foundKey = '0';

            foreach (KeyValuePair<char, int> kvp in ConsonantPile) {
                if (randomNumber < kvp.Value) {
                    foundKey = kvp.Key;
                    ConsonantPile[kvp.Key] = kvp.Value - 1;
                    totalConsonantWeight += -1;
                    break;
                }

                randomNumber = randomNumber - kvp.Value;
            }

            return foundKey;
        }
    }
}

