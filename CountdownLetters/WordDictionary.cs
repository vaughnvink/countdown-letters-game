﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace CountdownLetters {
    /// <summary>
    /// Class <c>WordDictionary</c> loads a txt file in which each line specifies an allowed word in the game.
    /// Contains methods for gaining the longest possible word and also checking whether a word is valid.
    /// 
    /// </summary>
    public class WordDictionary {

        private Dictionary<String, ArrayList> dict;

        public WordDictionary() {
            dict = new Dictionary<String, ArrayList>();
            LoadFile();
        }

        /// <summary>
        /// Method <c>PrintDictionary</c> prints the entire dictionary to the console.
        /// </summary>
        public void PrintDictionary() {
            foreach (KeyValuePair<String, ArrayList> kvp in dict) {
                Console.WriteLine("Key = {0}, Value = {1}", kvp.Key, CollapseArrayList(kvp.Value));
            }
        }

        /// <summary>
        /// Method <c>CollapseArrayList</c> collapses ArrayList into a String, seperator between values is a single space.
        /// </summary>
        /// <param name="list">ArrayList to collapse</param>
        /// <returns>Collapsed list as String</returns>
        private String CollapseArrayList(ArrayList list) {
            String collapsed = "";
            foreach (String i in list) {
                collapsed += " " + i;
            }
            return collapsed;
        }
        
        /// <summary>
        /// Method <c>Solve</c> finds the longest words that uses the same letters as the input String.
        /// (!) Disabled returning multipe solutions, solve ends when it finds the first global maximum.
        /// </summary>
        /// <param name="letters">String which contains all available letters.</param>
        /// <returns>Array of Strings containing global maximums from the search space.</returns>
        public String[] Solve(String letters) {
            if ((letters is null) || (letters.Length != GameSettings.maximumLetters)) {
                throw new ArgumentOutOfRangeException(nameof(letters));
            }
            //Get all combinations of the input pattern.
            String[] returned = Combination2(letters);
            String longestWord = "";

            foreach (var item in returned) {
                if (dict.ContainsKey(item)) {
                    
                    String word = (String) dict[item][0];
                    if (word.Length >= longestWord.Length) {
                        longestWord = word;
                    }
                }
            }

            return new String[] { longestWord };
        }

        /// <summary>
        /// Method <c>IsWordInDictionary</c> check whether a word is present in the dictionary. Returns boolean.
        /// </summary>
        /// <param name="word">String to check</param>
        /// <returns>bool is present in dictionary</returns>
        public bool IsWordInDictionary(String word) {
            Boolean found = false;
            String wordSorted = String.Concat(word.OrderBy(c => c));
            if (dict.ContainsKey(wordSorted)) {
                foreach (String wordInWordList in dict[wordSorted]) {
                    if (wordInWordList.Equals(word)) {
                        found = true;
                    }
                }
            }
            return found;
        }

        /// <summary>
        /// Method <c>LoadFile</c> loads a text file into the dictionary.
        /// Keys are alphabetically inner-sorted anagrams of words, values being the words that are valid for this anagram.
        /// </summary>
        public void LoadFile() {
            if (!File.Exists(GameSettings.corpusPath)) {
                Console.WriteLine("(!) Warning: Could not load dictionary! Press enter to continue.");
                Console.ReadLine();
                return;
            }
            dict = new Dictionary<String, ArrayList>();
            const Int32 BufferSize = 128;
            using (var fileStream = File.OpenRead(GameSettings.corpusPath))
            using (var streamReader = new StreamReader(fileStream, Encoding.UTF8, true, BufferSize)) {
                String line;
                String sortedLine;
                // Process line
                while ((line = streamReader.ReadLine()) != null) {
                    sortedLine = String.Concat(line.OrderBy(c => c));
                    if (dict.ContainsKey(sortedLine)) {
                        dict[sortedLine].Add(line);
                    } else {
                        dict.Add(sortedLine, new ArrayList() { line });
                    }
                }
            }
        }

        /// <summary>
        /// Method <c>Combination2</c> generates an array of String which are all possible permutations based on the order of the
        /// input String's characters.
        /// </summary>
        /// <param name="str">String to calculate.</param>
        /// <returns>String Array of all permutations.</returns>
        public static string[] Combination2(string str) {
            List<string> output = new List<string>();
            // Working buffer to build new sub-strings
            char[] buffer = new char[str.Length];

            Combination2Recurse(str.ToCharArray(), 0, buffer, 0, output);

            return output.ToArray();
        }

        /// <summary>
        /// Recursive method used for Combination2
        /// </summary>
        public static void Combination2Recurse(char[] input, int inputPos, char[] buffer, int bufferPos, List<string> output) {
            if (inputPos >= input.Length) {
                // Add only non-empty strings
                if (bufferPos > 0)
                    output.Add(String.Concat(new string(buffer, 0, bufferPos).OrderBy(c => c)));

                return;
            }

            // Recurse 2 times - one time without adding current input char, one time with.
            Combination2Recurse(input, inputPos + 1, buffer, bufferPos, output);

            buffer[bufferPos] = input[inputPos];
            Combination2Recurse(input, inputPos + 1, buffer, bufferPos + 1, output);
        }
    }
}