﻿using CountdownLetters;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CountDownLetters {
    /// <summary>
    /// Class <c>UserPile</c> extends LetterPile. Contains the vowels and consonants the user has picked.
    /// Contains both methods to get flags from the UserPile
    /// and a method check whether a String is using the same characters as the Userpile.
    /// Uses GameSettings for limit values.
    /// </summary>
    public class UserPile : LetterPile {
        public UserPile() {
            //The UserPile's characters sorted by the time added.
            SequentialUserPile = "";

            VowelPile = new Dictionary<char, int>();
            ConsonantPile = new Dictionary<char, int>();
        }

        public String SequentialUserPile { get; set; }

        /// <summary>
        /// Method <c>Add</c> adds a character to the UserPile. Returns boolean based on operation succes.
        /// </summary>
        /// <param name="character">(char) Character to add to UserPile</param>
        /// <returns>(bool) Operation successful</returns>
        public bool Add(char character) {
            if (!CanAddMoreLetters()) {
                return false;
            }

            bool success = false;
            if (GameSettings.vowelSettings.ContainsKey(character) && CanAddMoreVowels()) {
                if (VowelPile.ContainsKey(character)) {
                    VowelPile[character] += 1;
                } else {
                    VowelPile.Add(character, 1);
                }
                SequentialUserPile += character;
                success = true;
            } else if (GameSettings.consonantSettings.ContainsKey(character) && CanAddMoreConsonants()) {
                if (ConsonantPile.ContainsKey(character)) {
                    ConsonantPile[character] += 1;
                } else {
                    ConsonantPile.Add(character, 1);
                }
                SequentialUserPile += character;
                success = true;
            }
            return success;
        }
        /// <summary>
        /// Method <c>IsWordAccordingToPile</c> checks whether a String follows the same pattern as a UserPile.
        /// </summary>
        /// <param name="word">String to check</param>
        /// <returns>(bool) Whether String uses only available characters.</returns>
        public bool IsWordAccordingToPile(String word) {
            bool ok = true;
            String sequentialLowerCase = SequentialUserPile.ToLower();
            foreach (char c in word) {
                if (!sequentialLowerCase.Contains(c)) {
                    ok = false;
                }
            }
            return ok;
        }

        //Simple flag functions, used for checking limits while adding.
        public bool CanAddMoreLetters() {
            return (GetPileLength(VowelPile) + GetPileLength(ConsonantPile)) < GameSettings.maximumLetters;
        }
        public bool CanAddMoreVowels() {
            return GetPileLength(VowelPile) < (GameSettings.maximumLetters - GameSettings.minimumConsonants);
        }
        public bool CanAddMoreConsonants() {
            return GetPileLength(ConsonantPile) < (GameSettings.maximumLetters - GameSettings.minimumVowels);
        }
        public override String ToString() {
            return SequentialUserPile;
        }
    }
}
