﻿using System.Collections.Generic;

namespace CountDownLetters {
    /// <summary>
    /// Class <c>LetterPile</c> is the basic implementation of a set of two collections which are
    /// distinct in the kinds of letters they can contain.
    /// </summary>
    public class LetterPile {
        public LetterPile() {
        }

        //Two piles(collections) for Vowels and Consonants.
        protected Dictionary<char, int> VowelPile { get; set; }
        protected Dictionary<char, int> ConsonantPile { get; set; }

        /// <summary>
        /// Method <c>GetPileLength</c> calculates and returns the Length(=sum of all values) of a pile.
        /// </summary>
        /// <param name="pile">Pile to calculate Length of.</param>
        /// <returns>(int) Length of Pile.</returns>
        public int GetPileLength(Dictionary<char, int> pile) {
            int totalLength = 0;
            foreach (KeyValuePair<char, int> kvp in pile) {
                totalLength += kvp.Value;
            }
            return totalLength;
        }
    }
}

